#pragma once

#include <vector>

#include "Ramp.h"
#include "Adsr.h"
#include "SineOsc.h"

namespace UnSinte
{

class Dsp
{
public:
    enum class Parameter
    {
        frequency,
        gain
    };

    float sampleRate;

    explicit Dsp();
    virtual ~Dsp();

    void noteOn();
    void noteOff();
    void start (double sampleRate, int samplesPerBlock);
    void stop();
    void process (float* const* outputs, int numSamples);
    void setParameter(Parameter p, float value);

private:
    Ramp frequencyRamp;
    std::vector<float> frequencyRampOut;
    Ramp gainRamp;
    std::vector<float> gainRampOut;
    SineOsc oscillator;
    Adsr adsr;
    std::vector<float> adrsOut;

    // allow no copies:
    Dsp (const Dsp&);
    Dsp& operator= (const Dsp&);
};

} // namespace UnSinte
