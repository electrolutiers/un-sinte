#include <cmath>
#include <iostream>
#include <juce_core/juce_core.h>

#include "Dsp.h"
#include "Ramp.h"

namespace UnSinte
{

Ramp::Ramp (Dsp& parentDsp) 
    : dsp (parentDsp)
{
    time = 0.005f;
    targetValue = 0;
    currentValue = 0;
    increment = 0;
}

Ramp::~Ramp() {}

void Ramp::setTime (float t)
{
    time = t;
    calculateIncrement();
}

void Ramp::setTargetValue (float value)
{
    if (value == targetValue)
    {
        return;
    }
    targetValue = value;
    calculateIncrement();
}

void Ramp::setCurrentValue (float value)
{
    currentValue = value;
    targetValue = value;
    increment = 0;
    calculateIncrement();
}

void Ramp::calculateIncrement()
{
    float numSamples = dsp.sampleRate * time;
    increment = (targetValue - currentValue) / numSamples;
}

void Ramp::process (float* output, int n)
{
    for (int i = 0; i < n; ++i)
    {
        currentValue = currentValue + increment;

        if ((increment < 0 && currentValue <= targetValue)
            || (increment > 0 && currentValue >= targetValue))
        {
            increment = 0;
            currentValue = targetValue;
        }

        output[i] = currentValue;
    }
}

} // namespace UnSinte
