#pragma once

// clang-format off

/**
 * This file contains some platform specific definitions. 
 * It helps keep our code-base platform independent
 */


 /**
  * This is the good old C++ trick for creating a macro that forces the user to
  * put a semicolon after it when they use it.
  */
#define _MACRO_FUNCTION_(x) do { x } while (false)

#ifndef NDEBUG
    #ifdef JucePlugin_IsSynth  
        // Compiling as a juce plugin (doesn't matter if it's a synth or not).
        #include <juce_core/juce_core.h>

        /**
         * Logs an assertion message if condition x is false. Ex:
         *   ASSERT( someVariable < SOME_MAX_VALUE );
         */
        #define ASSERT(x) _MACRO_FUNCTION_( jassert(x); )

        /**
         * If the program is running within a debugger, pauses execution.
         */
        #define DEBUGGER_BREAK(x) _MACRO_FUNCTION_( \
            if (juce::juce_isRunningUnderDebugger()) JUCE_BREAK_IN_DEBUGGER; \
        )

        
    #endif
#else
    #define ASSERT(x) _MACRO_FUNCTION_( ; )
    #define DEBUGGER_BREAK(x) _MACRO_FUNCTION_( ; )
#endif


/**
 * Just print a string. Ex:
 *   DEBUG_PRINT << "something " << 2.0;
 */
#ifndef NDEBUG
    #ifdef JucePlugin_IsSynth  
        #include <iostream>
        struct DEBUG_LOGGER {
            ~DEBUG_LOGGER() { std::cout << std::endl; }
        };
        #define DEBUG_PRINT ( DEBUG_LOGGER(), std::cout )
    #endif 
#else
    #define DEBUG_PRINT if(true) {} else std::cout 
#endif
