#pragma once

namespace UnSinte
{

class Dsp;

class Ramp
{
public:
    Ramp (Dsp& parentDsp);
    ~Ramp();

    void setTime (float time);
    void setTargetValue (float value);
    void setCurrentValue (float value);

    void process (float* output, int n);

private:
    Dsp& dsp;

    float time;
    float increment;
    float targetValue;
    float currentValue;

    void calculateIncrement();
};

} // namespace UnSinte
