#pragma once

namespace UnSinte
{

class Dsp;

class SineOsc
{
public:
    SineOsc (Dsp& parentDsp);
    ~SineOsc();

    void process (float* inputFreq, float* output, int bufferSize);

private:
    Dsp& dsp;
    static const int tableSize = 512;
    static bool tableFull;
    static float table[tableSize + 2];
    float index = 0.0;

    void initTable();
};

} // namespace UnSinte
