#include "Adsr.h"
#include "Dsp.h"
#include "Definitions.h"

namespace UnSinte
{

Adsr::Adsr(Dsp& parentDsp)
    : dsp(parentDsp), //
      state(State::idle),
      currentValue(0)
{
}

Adsr::~Adsr() {}

void Adsr::start()
{
    state = State::idle;
    setAttack(1000);
    setDecay(2000);
    setSustain(0.5);
    setRelease(3000);
}

void Adsr::on()
{
    state = State::attack; //
}

void Adsr::off()
{
    state = State::release; //
}

void Adsr::setAttack(float newAttack)
{
    if(newAttack == attack)
    {
        return;
    }
    attack = newAttack;
    float numSamples = dsp.sampleRate * attack / 1000.0f;
    attackIncrement = 1.0f / numSamples;
}

void Adsr::setDecay(float newDecay)
{
    if(newDecay == decay)
    {
        return;
    }
    decay = newDecay;
    float numSamples = dsp.sampleRate * decay / 1000.0f;
    decayIncrement = 1.0f / numSamples;
}

void Adsr::setSustain(float newSustain)
{
    if(newSustain == sustain)
    {
        return;
    }
    sustain = newSustain;
}

void Adsr::setRelease(float newRelease)
{
    if(newRelease == release)
    {
        return;
    }
    release = newRelease;
    float numSamples = dsp.sampleRate * release / 1000.0f;
    releaseIncrement = 1.0f / numSamples;
}

void Adsr::process(float* output, int n)
{
    if(state == State::idle)
    {
        for(int i = 0; i < n; ++i)
        {
            output[i] = 0.0;
        }
    }
    else if(state == State::attack)
    {
        for(int i = 0; i < n; ++i)
        {
            currentValue += attackIncrement;
            if(currentValue >= 1.0)
            {
                currentValue = 1.0;
                state = State::decay;
            }
            output[i] = currentValue;
        }
    }
    else if(state == State::decay)
    {
        for(int i = 0; i < n; ++i)
        {
            currentValue -= decayIncrement;
            if(currentValue <= sustain)
            {
                currentValue = sustain;
                state = State::sustain;
            }
            output[i] = currentValue;
        }
    }
    else if(state == State::sustain)
    {
        for(int i = 0; i < n; ++i)
        {
            output[i] = currentValue;
        }
    }
    else if(state == State::release)
    {
        for(int i = 0; i < n; ++i)
        {
            currentValue -= releaseIncrement;
            if(currentValue <= 0.0)
            {
                currentValue = 0.0;
                state = State::idle;
            }
            output[i] = currentValue;
        }
    }
}

} // namespace UnSinte
