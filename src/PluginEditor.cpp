#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
AudioPluginAudioProcessorEditor::AudioPluginAudioProcessorEditor (AudioPluginAudioProcessor& p)
    : AudioProcessorEditor (&p), processorRef (p)
{
    juce::ignoreUnused (processorRef);
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (400, 300);
    configureSlider(frequencySlider,20.f, 20000.f);
    addAndMakeVisible(frequencySlider);
    configureSlider(gainSlider,-60.f,6.f);
    addAndMakeVisible(gainSlider);

    freqAttachment = std::make_unique<SliderAttachment>(processorRef.parameters,
                                                        "frequency",
                                                        frequencySlider);
    gainAttachment = std::make_unique<SliderAttachment>(processorRef.parameters,
                                                    "gain",
                                                    gainSlider);

    startTimerHz(30);
}

AudioPluginAudioProcessorEditor::~AudioPluginAudioProcessorEditor()
{
}

//==============================================================================
void AudioPluginAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));

}

void AudioPluginAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..

    frequencySlider.setBounds(0, 0, 200, 200);
    gainSlider.setBounds(200,0,200,200);
}
void AudioPluginAudioProcessorEditor::timerCallback()
{
    //TODO: el rango de la frecuencia debe estar en escala logarítmica.
    frequencySlider.setValue(processorRef.parameters.getRawParameterValue("frequency")->load(),
                             juce::dontSendNotification);
    frequencySlider.repaint();
}

void AudioPluginAudioProcessorEditor::configureSlider(juce::Slider &slider, float min, float max)
{
    slider.setRange(min, max);
    slider.setSliderStyle(juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    slider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::TextBoxBelow,
                                    true,
                                    60,
                                    30);

}

