#include "Dsp.h"

#define ADSR 0
namespace UnSinte
{

Dsp::Dsp() : frequencyRamp(*this), gainRamp(*this), oscillator(*this), adsr(*this) {}

Dsp::~Dsp() {}

void Dsp::setParameter(Parameter p, float value)
{
    switch (p)
    {
        case Parameter::frequency:
            frequencyRamp.setTargetValue(value);
            break;
        case Parameter::gain:
            gainRamp.setTargetValue(value);
            break;

    }
}

void Dsp::start(double newSampleRate, int samplesPerBlock)
{
    sampleRate = (float) newSampleRate;
    frequencyRampOut.resize((unsigned long) samplesPerBlock);
    gainRampOut.resize(samplesPerBlock);

    adsr.start();
    adrsOut.resize((unsigned long) samplesPerBlock);
}

void Dsp::stop() {}

void Dsp::noteOn()
{
    adsr.on(); //
}

void Dsp::noteOff()
{
    adsr.off(); //
}

void Dsp::process(float* const* outputs, int numSamples)
{
    float* outputBuffer = outputs[0];
    frequencyRamp.process(frequencyRampOut.data(), numSamples);
    gainRamp.process(gainRampOut.data(),numSamples);
    oscillator.process(frequencyRampOut.data(), outputBuffer, numSamples);
    adsr.process(adrsOut.data(), numSamples);

    for (int i = 0; i < numSamples; ++i) {

        outputBuffer[i] *= gainRampOut[(unsigned long)i];
        outputBuffer[i] *= adrsOut[(unsigned long)i];
    }
}

} // namespace UnSinte
