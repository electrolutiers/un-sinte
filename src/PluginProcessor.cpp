#include "PluginProcessor.h"
#include "PluginEditor.h"

#include "Definitions.h"

#include <cmath>

//==============================================================================
AudioPluginAudioProcessor::AudioPluginAudioProcessor()
    : AudioProcessor(BusesProperties().withOutput("Output", juce::AudioChannelSet::mono(), true)),
      parameters(*this, nullptr, "Parameters", createParameters()),
      currentFrequency(0)
{
    frequencyParameter = parameters.getRawParameterValue("frequency");
    gainParameter = parameters.getRawParameterValue("gain");
    currentFrequency = 440.f;
    currentGain = 1.f;
}

AudioPluginAudioProcessor::~AudioPluginAudioProcessor() {}

void AudioPluginAudioProcessor::prepareToPlay(double sampleRate, int samplesPerBlock)
{
    dsp.start(sampleRate, samplesPerBlock);
    currentFrequency = *frequencyParameter;
    dsp.setParameter(UnSinte::Dsp::Parameter::frequency, currentFrequency);
}

void AudioPluginAudioProcessor::processBlock(juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    if(*frequencyParameter != currentFrequency)
    {
        currentFrequency = *frequencyParameter;
        dsp.setParameter(UnSinte::Dsp::Parameter::frequency, currentFrequency);
    }
    if (*gainParameter != currentGain)
    {
        currentGain  = *gainParameter;
        dsp.setParameter(UnSinte::Dsp::Parameter::gain,
                         juce::Decibels::decibelsToGain(currentGain));
    }

    for(const auto metadata : midiMessages)
    {
        auto message = metadata.getMessage();
        if(message.isNoteOn())
        {
            *frequencyParameter = (juce::MidiMessage::getMidiNoteInHertz(message.getNoteNumber()));
            dsp.noteOn();
        }
        else if(message.isNoteOff())
        {
            dsp.noteOff();
        }
    }

    if(*frequencyParameter != currentFrequency)
    {
        currentFrequency = *frequencyParameter;
        dsp.setParameter(UnSinte::Dsp::Parameter::frequency, currentFrequency);
    }

    dsp.process(buffer.getArrayOfWritePointers(), buffer.getNumSamples());
}

//==============================================================================
const juce::String AudioPluginAudioProcessor::getName() const { return JucePlugin_Name; }

bool AudioPluginAudioProcessor::acceptsMidi() const { return true; }

bool AudioPluginAudioProcessor::producesMidi() const { return false; }

bool AudioPluginAudioProcessor::isMidiEffect() const { return false; }

double AudioPluginAudioProcessor::getTailLengthSeconds() const { return 0.0; }

int AudioPluginAudioProcessor::getNumPrograms()
{
    return 1; // NB: some hosts don't cope very well if you tell them there are 0 programs,
              // so this should be at least 1, even if you're not really implementing programs.
}

int AudioPluginAudioProcessor::getCurrentProgram() { return 0; }

void AudioPluginAudioProcessor::setCurrentProgram(int index) { juce::ignoreUnused(index); }

const juce::String AudioPluginAudioProcessor::getProgramName(int index)
{
    juce::ignoreUnused(index);
    return {};
}

void AudioPluginAudioProcessor::changeProgramName(int index, const juce::String& newName)
{
    juce::ignoreUnused(index, newName);
}

//==============================================================================

void AudioPluginAudioProcessor::releaseResources() { dsp.stop(); }

bool AudioPluginAudioProcessor::isBusesLayoutSupported(const BusesLayout& layouts) const
{
    // only allow stereo and mono
    if(layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono())
    {
        return false;
    }
    return true;
}

//==============================================================================
bool AudioPluginAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* AudioPluginAudioProcessor::createEditor()
{
    //return new juce::GenericAudioProcessorEditor(*this);
    return new AudioPluginAudioProcessorEditor(*this);
}

//==============================================================================
void AudioPluginAudioProcessor::getStateInformation(juce::MemoryBlock& destData)
{
    auto state = parameters.copyState();
    std::unique_ptr<juce::XmlElement> xml (state.createXml());
    copyXmlToBinary (*xml, destData);
}

void AudioPluginAudioProcessor::setStateInformation(const void* data, int sizeInBytes)
{
    std::unique_ptr<juce::XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));

    if (xmlState != nullptr)
        if (xmlState->hasTagName (parameters.state.getType()))
        {
            parameters.replaceState(juce::ValueTree::fromXml(*xmlState));
        }
}

juce::AudioProcessorValueTreeState::ParameterLayout AudioPluginAudioProcessor::createParameters()
{
    std::vector<std::unique_ptr<juce::RangedAudioParameter>> params;

    params.push_back(
        std::make_unique<juce::AudioParameterFloat>("frequency",
                                                    "Frequency",
                                                    20.0,
                                                    20000.0,
                                                    440.0));
    juce::NormalisableRange gainRange (-60.f, 6.0f);
    params.push_back(
            std::make_unique<juce::AudioParameterFloat>("gain",
                                                        "Gain",
                                                        gainRange,
                                                        0.f)
                                                        );
    return {params.begin(), params.end()};
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter() { return new AudioPluginAudioProcessor(); }
