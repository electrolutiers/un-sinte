#pragma once

namespace UnSinte
{

class Dsp;

class Adsr
{
public:
    enum class State
    {
        idle,
        attack,
        decay,
        sustain,
        release
    };

    explicit Adsr(Dsp& parentDsp);
    virtual ~Adsr();

    void start();

    /*
     * @brief Triggers the start of a ADRS cycle (note on)
     */
    void on();

    /*
     * @brief Triggers the start of a ADRS cycle (note off)
     */
    void off();

    /*
     * @brief sets the attack in milliseconds
     */
    void setAttack(float attack);

    /*
     * @brief sets the decay in milliseconds
     */
    void setDecay(float decay);

    /*
     * @brief sets the sustain in the range [0,1]
     */
    void setSustain(float sustain);

    /*
     * @brief sets the release in milliseconds
     */
    void setRelease(float release);

    void process(float* output, int n);

private:
    Dsp& dsp;
    State state;

    float currentValue;
    float attack;  
    float attackIncrement;  
    float decay;
    float decayIncrement;  
    float sustain;
    float release;
    float releaseIncrement;  

    // no copies
    Adsr(const Adsr&);
    Adsr& operator=(const Adsr&);
};

} // namespace UnSinte
