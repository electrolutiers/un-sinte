# Un Sinte.

Este repositorio hace parte del trabajo de Electrolutiers, mira nuestra [wiki](https://gitlab.com/electrolutiers/wiki) para mas info.

Este es un sintetizador escrito en C++. La idea es escribir la parte de sintesis como código de C++ sin dependencias de sistema operativo, escribir una interfaz gráfica y un plugin en JUCE y luego llevar la parte de sintesis a una plataforma embebida que puede ser Daisy Seed.

## Cómo compilar y correr el plugin en Linux.

Es necesario tener instalado previamente cmake, ninja, gcc. En Arch Linux todo eso se instala con `sudo pacman -S base-devel cmake ninja`.

Para compilar el plugin la primera vez:

```
git clone https://gitlab.com/electrolutiers/un-sinte.git
cd un-sinte
git submodule update --init --recursive
mkdir build
cd build
cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug ..
ninja
```

Las siguientes veces:

```
cd un-sinte/build
ninja
```

Para correr y probar el plugin es necesario cargarlo en alguna aplicación host que permita cargar plugins VST3. Juce viene con uno incluido. Para compilarlo: 

```
cd vendor/juce/extras/AudioPluginHost/Builds/LinuxMakefile
make
```

Luego para correrlo, 
```
cd vendor/juce/extras/AudioPluginHost/Builds/LinuxMakefile
./AudioPluginHost
```
Ahi es necesario agregar la ruta a donde está el plugin compilado y cargarlo, al cerrar el plugin host, preguntará si quiere guardar, digale que si y la proxima vez que lo abra, el plugin ya estará cargado. 

## Cómo compilar y correr el plugin en Windows.

Es necesario tener instalado previamente cmake, PowerShell, Visual Studio. Todos son gratuitos disponibles en la página de cada desarrollador. 

Para compilar el plugin la primera vez:

```
git clone https://gitlab.com/electrolutiers/un-sinte.git
cd un-sinte
git submodule update --init --recursive
mkdir build
cd build
cmake ..
cmake --build.
```
Para correr y probar el plugin es necesario cargarlo en alguna aplicación host que permita cargar plugins VST3. Juce viene con uno incluido. Para compilarlo se abre el archivo de Visual Studio de la carpeta: 
```
vendor/juce/extras/AudioPluginHost/Builds/VisualStudioXX
```
Abrir la versión de software correspondiente, y compilar.

Luego para correrlo, 
```
vendor/juce/extras/AudioPluginHost/Builds/VisualStudioXX/.../Debug/App/AudioPlugginHost
```
Ahi es necesario agregar la ruta a donde está el plugin compilado y cargarlo 
(posiblemente en C:\Program Files\Common Files\VST3), 
al cerrar el plugin host, preguntará si quiere guardar, digale que si y la proxima vez que lo abra, el plugin ya estará cargado. 


## Licencia.

Todo el código de este proyecto está cobijado por la licencia GPLv3.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.









